package com.training.collection.examples;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;
import java.util.Vector;

public class CollectionListExample {
	
	

	public static void main(String[] args) {
		
		
		int[] ab = {1,2,4,5};
	
		//a[7] = 10;
		
		
		
		//ArrayList
		// it's a dynamic array
		// it's easy to remove and store values
		// we can store duplicate data in ArrayList or List
		
		//example of Arraylist
		
		ArrayList<String> list = new ArrayList<>();
		
		// adding an element
		list.add("Aradhana"); //0
		list.add("Bhupesh");  //1
		list.add("Rajshree");  //2
		list.add("Samjhan");  //3
		list.add("Aradhana");  //4
		
		
	//	System.out.println(list);
	
		//removing an element 
		//list.remove(0);
		
	//	System.out.println(list);
		
		// clearing the whole list
		//list.clear();
		
	//	System.out.println(" no list available: " +list);
		
		// this is one of iterating over the List 
	//	Iterator<String> iterate = list.iterator();
		
	//	while(iterate.hasNext()) {
			
		//	System.out.println(iterate.next());
		
	//	}
		
		// for loop
		for(String name: list ) {
			
			if(name.equalsIgnoreCase("Aradhana")) {
			//	System.out.println(name);
			}
			
		//	System.out.println(name);
		}
		
		
		
		//LinkedList
		
		LinkedList<String> linkedList = new LinkedList<>();
		
		// adding an element
		linkedList.add("Aradhana"); //0
		linkedList.add("Bhupesh");  //1
		linkedList.add("Rajshree");  //2
		linkedList.add("Samjhan");  //3
		linkedList.add("Aradhana");  //4
		
		
		//System.out.println(linkedList);
	
		//removing an element 
		linkedList.remove(0);
		
	//	System.out.println(list);
		
		// clearing the whole list
		linkedList.clear();
		
	//	System.out.println(" no list available: " +linkedList);
		
		// this is one of iterating over the List 
//		Iterator<String> iterate = linkedList.iterator();
//		
//		while(iterate.hasNext()) {
//			
//			System.out.println(iterate.next());
//		
//		}
		
		// for loop
//		for(String name: linkedList ) {
//			
//			if(name.equalsIgnoreCase("Aradhana")) {
//				System.out.println(name);
//			}
//			
//			System.out.println(name);
//		}
		
			//Vector
				Vector<String> vectorExample = new Vector<>();
				
				// adding an element
				vectorExample.add("Aradhana"); //0
				vectorExample.add("Bhupesh");  //1
				vectorExample.add("Rajshree");  //2
				vectorExample.add("Samjhan");  //3
				vectorExample.add("Aradhana");  //4
				
				
				System.out.println(vectorExample);
			
				//removing an element 
				vectorExample.remove(0);
				
			//	System.out.println(list);
				
				// clearing the whole list
				vectorExample.clear();
				
			//	System.out.println(" no list available: " +linkedList);
				
				// this is one of iterating over the List 
//				Iterator<String> iter = vectorExample.iterator();
//				
//				while(iter.hasNext()) {
//					
//					System.out.println(iter.next());
//				
//				}
				
				// for loop
//				for(String name: vectorExample ) {
//					
//					if(name.equalsIgnoreCase("Aradhana")) {
//						System.out.println(name);
//					}
//					
//					System.out.println(name);
//				}
//		
				
				
		// Stack 
		// last - in -first out basis maa data access garcha
				
		Stack<String> stack = new Stack<>();
		stack.add("Raman");
		stack.push("Ram");
		stack.push("Shyam");
		stack.push("Hari");
		
		System.out.println(stack.peek());
		
		stack.pop(); // removes the head
		
		System.out.println(stack.peek());
		
	//	System.out.println(stack);
		
		

	}

}
