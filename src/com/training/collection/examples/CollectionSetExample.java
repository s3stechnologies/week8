package com.training.collection.examples;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class CollectionSetExample {

	
	
	
	public static void main(String[] args) {
		
		//set 
			// no duplicate values
			// can only have one null value
			// unordered elements
		
		
		//Set<String> s = new HashSet<>();
		
		//Set<String> s1 = new LinkedHashSet<>();
	
		//Set<String> s2 = new TreeSet<>();
		
		
		//Hashset 
		// has table is used to store elements
//		HashSet<String> s = new HashSet<>();
//		
//		s.add(null);
//		s.add("Ram");
//		s.add("Shyam");
//		s.add("Hari");
//		s.add("Hari");
//		s.add("Hari");
//		
//		s.add(null);
//		
//		Iterator<String> itera = s.iterator();
//		while(itera.hasNext()) {
//	//		System.out.println(itera.next());
//		}
		
		
//		for(String name : s) {
//			
//		//	System.out.println(name);
//		}
		
		
		// LinkedHashSet
		// insertion order is maintained
		// null elements are permits
		LinkedHashSet<String> s1 = new LinkedHashSet<>();
	
		
		
		s1.add("Hari");
		s1.add("Ram");
		s1.add("Shyam");
		
		
		//s1.add("Hari");
		//s1.add("Hari");
		
		s1.add(null);
		
//		Iterator<String> iterat = s.iterator();
//		while(iterat.hasNext()) {
//			System.out.println(iterat.next());
//		}
//		
		
		//for(String name1 : s1) {
			
		//	System.out.println(name1);
		//}
		
		
		//TreeSet
		// sorted in ascending order
		// no null values
		
		//SortedSet<String> s2 = new TreeSet<>();
		
//		TreeSet<String> s2 = new TreeSet<>();
//	
//
//		s2.add("Banana");
//		s2.add("Apple");
//		s2.add("Car");
		
		
		TreeSet<Integer> s2 = new TreeSet<>();
		

		s2.add(1);
		s2.add(5);
		s2.add(2);
		
		//s1.add("Hari");
		//s1.add("Hari");
		
	//	s2.add(null);
		
//		Iterator<String> iterat = s.iterator();
//		while(iterat.hasNext()) {
//			System.out.println(iterat.next());
//		}
//		
		
		for(int name2 : s2) {
			
			System.out.println(name2);
		}
		
		
	}

}
