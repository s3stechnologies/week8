package com.training.collection.examples;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.PriorityQueue;
import java.util.Queue;

public class CollectionQueueExample {

	
	
	public static void main(String[] args) {
		
		//Queue interface
		
		// FiFO - First in First Out
		
		
		//Queue<String> que = new PriorityQueue<>();

		// PriorityQueue
		PriorityQueue<String> que = new PriorityQueue<>();
		
		que.add("Ram");
		que.add("Shyam");
	que.add("Hari");
		
	//	System.out.println(que.element());
		
	//	System.out.println(que.peek());
		
		for(String name: que) {
		
			
	//	System.out.println(name);
		
		}
		
		
		
		// ArrayDeque 
		// Implements Queue and Linkedlist
		
		//Deque<String> deq = new ArrayDeque<>();
		
		ArrayDeque<String> deq = new ArrayDeque<>();
		
		deq.add("Ram");
		deq.add("Shyam");
		deq.add("Hari");
		
		for(String name1 :deq ) {
			System.out.println(name1);
		}
		
		
	}

}
