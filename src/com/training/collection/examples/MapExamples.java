package com.training.collection.examples;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;


class Person{
	
	private String name;
	 private int age;
	
	
	public Person(String name, int age) {
		this.name = name;
		this.age = age;
		
	}


	public String getName() {
		return name;
	}


	public int getAge() {
		return age;
	}


	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + "]";
	}
	
	

}


class MapExamples {

	public static void main(String[] args) {
		
		
		// Values are saved in Key value pair
		// K, V
		// key should be unique
//		Map<Integer,String> name = new HashMap<>();
//		
//		//adding elements to the map
//		name.put(1, "Aradhana");
//		name.put(2, "Bhupesh");
//		name.put(3,  "Rajshree");
//		name.put(4, "Samjhan");
//		name.put(5,  "Rajendra");
//		name.put(6,  "Rajendra");
//		
//		
//		System.out.println(name);
//		
//		for(Map.Entry ent: name.entrySet()) {
//			System.out.println(ent.getKey() + " " + ent.getValue());
//		//	System.out.println(ent.getValue());
//		}
		
		
		Person ram = new Person("Ram", 29);
		Person shyam = new Person("Shyam", 30);
		Person hari = new Person("Hari", 40);
		Person biwash = new Person("Biwash", 19);
		
//		Map<Integer,Person> name = new HashMap<>();
//		name.put(1, ram);
//		name.put(2, shyam);
//		name.put(3, hari);
//		name.put(4, null);
//		name.put(5, null);
		
	//System.out.println(name.containsKey(1));
	//System.out.println(name.containsValue(biwash));
		
//		for(Map.Entry ent: name.entrySet()) {
//			System.out.println(ent.getKey() + " " + ent.getValue());
//			//System.out.println(ent.getValue());
//		}
//		
//		name.replace(2, biwash);
		
	//	name.remove(3, hari);
	
		
//		for(Map.Entry ent: name.entrySet()) {
//			System.out.println(ent.getKey() + " " + ent.getValue());
//			//System.out.println(ent.getValue());
//		}
		
		
		
		// Hashtable is synocrized 
		// you can't null values or keys as well
		// descending order

//		Hashtable<Integer,Person> name1 = new Hashtable<>();
//		
//		name1.put(1, ram);
//		name1.put(3, hari);
//		name1.put(2, shyam);
//		name1.put(5, biwash);
		
		//name1.put(4, null);
		//name1.put(5, null);
		
//	
//		for(Map.Entry ent: name1.entrySet()) {
//			System.out.println(ent.getKey() + " " + ent.getValue());
//			//System.out.println(ent.getValue());
//		}
		
		
		// TreeMap
		// no null Keys
		// contains unique keys
		//  not synchronized
		// ascending order 
		// implements sorted map
		
		//Map<Integer,Person> name2 = new TreeMap<>();
		
		
//		TreeMap<Integer,Person> name2 = new TreeMap<>();
//		
//		name2.put(1, ram);
//		name2.put(3, hari);
//		name2.put(2, shyam);
//		name2.put(5, biwash);
//		name2.put(6, ram);
//		
//		//name2.put(null, biwash );
//	//	name2.put(5, null);
//		
//	//	name2.remove(5);
//		
//	
//		System.out.println(name2.descendingMap());
//		
//		for(Map.Entry ent: name2.entrySet()) {
//			System.out.println(ent.getKey() + " " + ent.getValue());
//			//System.out.println(ent.getValue());
//		}
//		
		
		
		// maintains insertion order
		//uniques keys
		// null values can be multiple
		// only one null key
		LinkedHashMap<Integer,Person> name3 = new LinkedHashMap<>();
	
		name3.put(1, ram);
		name3.put(3, hari);
		name3.put(2, shyam);
		name3.put(5, biwash);
		name3.put(3, ram); // overrides
		name3.put(null, hari);
		name3.put(null, ram);
		
		
		
		//name2.put(null, biwash );
	//	name2.put(5, null);
		
	//	name2.remove(5);
	
		
		for(Map.Entry ent: name3.entrySet()) {
			System.out.println(ent.getKey() + " " + ent.getValue());
			//System.out.println(ent.getValue());
		}
		
		

	}

}
