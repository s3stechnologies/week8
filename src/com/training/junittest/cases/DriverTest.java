package com.training.junittest.cases;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
//import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DriverTest {
	
	Driver d;
	
	@Before
	public void init() {
		d = new Driver();
	}

//	@Test
//	public void drivingAgeValidatorTrueTest() {
//		
//		int age = 19;
//		
//		assertTrue(d.drivingAgeValidator(age));
//	}
//	
//	@Test
//	public void drivingAgeValidatorFalseTest() {
//		
//		int age = 17;
//		
//		assertFalse(d.drivingAgeValidator(age));
//	}
	
	
	@SuppressWarnings("deprecation")
	@Test
	public void drivingAgeValidatorTrueAsserThatTest() {
		
		int age = 17;
	
		assertNotNull(d.drivingAgeValidator(age));
		
		
		//assertThat(d.drivingAgeValidator(age),is(not("true")));
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void stringLengthPositiveTest() {
		
		String name = "Biwash";
	
		assertThat(name.length(),is(not(7)));
		
		//assertThat(name.length(),equals(7));
	}

}
