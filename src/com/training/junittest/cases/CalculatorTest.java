package com.training.junittest.cases;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;



public class CalculatorTest {
	
	Calculator c;
	
	
	@BeforeClass
	public static void runsBeforeWholeClass() {
	//	c = new Calculator();
		 
		// System.out.println("Before class is running!");
		
	}
	
	
	@Before
	public void runsBeforeTestMethod() {
		 c = new Calculator();
		 
		 System.out.println("Before is running!");
		
	}
	
	
	
	@Test
	public void addNumPositiveTest() {
			
		//int num1 = 10;
		//int num2 = 30;
		
		//Assert.assertEquals( 40 , c.addNum(num1, num2));
		
		System.out.println("Positive Test method of add is running!");
	
	}
	
	
	@Test
	public void addNumNegativeTest() {
	
		int num1 = 10;
		int num2 = 30;
		
		assertNotEquals(50, c.addNum(num1, num2));
		
		System.out.println("Negative Test method of add is running!");
		
		Assert.assertEquals( 50 , c.addNum(num1, num2));
	
	}
	
//	
//	@Test
//	public void doDivisionPositiveTest() {
//		
//	}
	
	
	@After
	public void runsAfterTestMethod() {
		// c = null;
		
		System.out.println("After is running!");
	}
	
	@AfterClass
	public static void runsAfterClass() {
		// c = null;
		
		System.out.println("After class is running!");
	}
	
	

}
